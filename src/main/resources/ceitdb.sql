/*
SQLyog Ultimate v8.32 
MySQL - 5.6.10-log : Database - ceit
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ceit` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ceit`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `account` */

insert  into `account`(`id`,`name`,`password`,`description`) values (1,'1111','111','1111');

/*Table structure for table `activity` */

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `activity` */

/*Table structure for table `ceitinfo` */

DROP TABLE IF EXISTS `ceitinfo`;

CREATE TABLE `ceitinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `notice` varchar(45) DEFAULT NULL,
  `teacher` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ceitinfo` */

/*Table structure for table `certification` */

DROP TABLE IF EXISTS `certification`;

CREATE TABLE `certification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `competdate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `competdate_id` (`competdate_id`),
  CONSTRAINT `certification_ibfk_1` FOREIGN KEY (`competdate_id`) REFERENCES `competdate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `certification` */

/*Table structure for table `competdate` */

DROP TABLE IF EXISTS `competdate`;

CREATE TABLE `competdate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `material` varchar(45) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `competdate_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `competdate` */

/*Table structure for table `competition` */

DROP TABLE IF EXISTS `competition`;

CREATE TABLE `competition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `proccess` varchar(45) DEFAULT NULL,
  `certification_id` int(11) DEFAULT NULL,
  `competype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `certification_id` (`certification_id`),
  KEY `competype_id` (`competype_id`),
  CONSTRAINT `competition_ibfk_1` FOREIGN KEY (`certification_id`) REFERENCES `certification` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `competition_ibfk_2` FOREIGN KEY (`competype_id`) REFERENCES `competype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `competition` */

/*Table structure for table `competype` */

DROP TABLE IF EXISTS `competype`;

CREATE TABLE `competype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `competype` */

/*Table structure for table `device` */

DROP TABLE IF EXISTS `device`;

CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `sncode` varchar(45) DEFAULT NULL,
  `madetime` datetime DEFAULT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `version` varchar(45) DEFAULT NULL,
  `devicetype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `devicetype_id` (`devicetype_id`),
  CONSTRAINT `device_ibfk_1` FOREIGN KEY (`devicetype_id`) REFERENCES `devicetype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `device` */

/*Table structure for table `devicetype` */

DROP TABLE IF EXISTS `devicetype`;

CREATE TABLE `devicetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `devicetype` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `resource` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parentMenu_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parentMenu_id` (`parentMenu_id`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parentMenu_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

/*Table structure for table `permission` */

DROP TABLE IF EXISTS `permission`;

CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `types` int(11) DEFAULT NULL,
  `resource` varchar(255) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `instance` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `resoursePerm` tinyint(1) DEFAULT NULL,
  `systemPerm` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8;

/*Data for the table `permission` */

insert  into `permission`(`id`,`types`,`resource`,`operation`,`instance`,`description`,`resoursePerm`,`systemPerm`) values (127,NULL,'index','*','*','棣栭〉',0,1),(128,NULL,'user','visit',NULL,'鐢ㄦ埛鍒楄〃',0,1),(129,NULL,'user','add',NULL,'娣诲姞鐢ㄦ埛',0,1),(130,NULL,'user','delete',NULL,'鍒犻櫎鐢ㄦ埛',0,1),(131,NULL,'user','edit',NULL,'淇敼鐢ㄦ埛',0,1),(132,NULL,'user','view',NULL,'鏌ョ湅鐢ㄦ埛',0,1),(133,NULL,'role','visit',NULL,'瑙掕壊鍒楄〃',0,1),(134,NULL,'role','add',NULL,'娣诲姞瑙掕壊',0,1),(135,NULL,'role','delete',NULL,'鍒犻櫎瑙掕壊',0,1),(136,NULL,'role','edit',NULL,'淇敼瑙掕壊',0,1),(137,NULL,'role','view',NULL,'鏌ョ湅瑙掕壊',0,1),(138,NULL,'user_message','add',NULL,'娣诲姞鐢ㄦ埛淇℃伅',0,1),(139,NULL,'user_message','get',NULL,'鑾峰彇鐢ㄦ埛淇℃伅',0,1),(140,NULL,'user_message','delete',NULL,'鍒犻櫎鐢ㄦ埛淇℃伅',0,1),(141,NULL,'user_message','edit',NULL,'淇敼鐢ㄦ埛淇℃伅',0,1),(142,NULL,'user_message','view',NULL,'鏌ョ湅鐢ㄦ埛淇℃伅',0,1),(143,NULL,'devicetype','list',NULL,'璁惧鍒楄〃鍒嗙被',0,1),(144,NULL,'devicetype','add',NULL,'娣诲姞璁惧鍒嗙被',0,1),(145,NULL,'devicetype','delete',NULL,'鍒犻櫎璁惧鍒嗙被',0,1),(146,NULL,'devicetype','edit',NULL,'淇敼璁惧鍒嗙被',0,1),(147,NULL,'devicetype','view',NULL,'鏌ョ湅璁惧鍒嗙被',0,1),(148,NULL,'projecttype','visit',NULL,'椤圭洰绫诲埆鍒楄〃',0,1),(149,NULL,'projecttype','add',NULL,'娣诲姞椤圭洰绫诲埆',0,1),(150,NULL,'projecttype','delete',NULL,'鍒犻櫎椤圭洰绫诲埆',0,1),(151,NULL,'projecttype','edit',NULL,'淇敼椤圭洰绫诲埆',0,1),(152,NULL,'projecttype','view',NULL,'鏌ョ湅椤圭洰绫诲埆',0,1),(153,NULL,'property','visit',NULL,'椤圭洰鍒楄〃',0,1),(154,NULL,'property','add',NULL,'娣诲姞椤圭洰',0,1),(155,NULL,'property','delete',NULL,'鍒犻櫎椤圭洰',0,1),(156,NULL,'property','edit',NULL,'淇敼椤圭洰',0,1),(157,NULL,'property','view',NULL,'鏌ョ湅椤圭洰',0,1),(158,NULL,'ceitinfo','add',NULL,'娣诲姞淇℃伅',0,1),(159,NULL,'ceitinfo','delete',NULL,'鍒犻櫎淇℃伅',0,1),(160,NULL,'ceitinfo','edit',NULL,'淇敼淇℃伅',0,1),(161,NULL,'ceitinfo','view',NULL,'鏌ョ湅淇℃伅',0,1),(162,NULL,'activity','visit',NULL,'娲诲姩鍒楄〃',0,1),(163,NULL,'activity','delete',NULL,'鍒犻櫎娲诲姩',0,1),(164,NULL,'activity','add',NULL,'娣诲姞娲诲姩',0,1),(165,NULL,'activity','view',NULL,'鏌ョ湅娲诲姩',0,1),(166,NULL,'account','visit',NULL,'鐣欒█鍒楄〃',0,1),(167,NULL,'account','delete',NULL,'鍒犻櫎鐣欒█',0,1),(168,NULL,'account','view',NULL,'鏌ョ湅鐣欒█',0,1),(169,NULL,'webtype','list',NULL,'鎶�鏈綉绔欏垪琛ㄥ垎绫�',0,1),(170,NULL,'webtype','add',NULL,'娣诲姞鎶�鏈綉绔欏垎绫�',0,1),(171,NULL,'webtype','delete',NULL,'鍒犻櫎鎶�鏈綉绔欏垎绫�',0,1),(172,NULL,'webtype','edit',NULL,'淇敼鎶�鏈綉绔欏垎绫�',0,1),(173,NULL,'webtype','view',NULL,'鏌ョ湅鎶�鏈綉绔欏垎绫�',0,1),(174,NULL,'web','visit',NULL,'鎶�鏈綉绔欏垪琛�',0,1),(175,NULL,'web','delete',NULL,'鍒犻櫎鎶�鏈綉绔�',0,1),(176,NULL,'web','edit',NULL,'淇敼鎶�鏈綉绔�',0,1),(177,NULL,'web','view',NULL,'鏌ョ湅鎶�鏈綉绔�',0,1),(178,NULL,'user','visit',NULL,'鐢ㄦ埛鍒楄〃',0,1),(179,NULL,'user','add',NULL,'娣诲姞鐢ㄦ埛',0,1),(180,NULL,'user','delete',NULL,'鍒犻櫎鐢ㄦ埛',0,1),(181,NULL,'user','edit',NULL,'淇敼鐢ㄦ埛',0,1),(182,NULL,'user','view',NULL,'鏌ョ湅鐢ㄦ埛',0,1),(183,NULL,'role','visit',NULL,'瑙掕壊鍒楄〃',0,1),(184,NULL,'role','add',NULL,'娣诲姞瑙掕壊',0,1),(185,NULL,'role','delete',NULL,'鍒犻櫎瑙掕壊',0,1),(186,NULL,'role','edit',NULL,'淇敼瑙掕壊',0,1),(187,NULL,'role','view',NULL,'鏌ョ湅瑙掕壊',0,1),(188,NULL,'user_message','add',NULL,'娣诲姞鐢ㄦ埛淇℃伅',0,1),(189,NULL,'user_message','get',NULL,'鑾峰彇鐢ㄦ埛淇℃伅',0,1),(190,NULL,'user_message','delete',NULL,'鍒犻櫎鐢ㄦ埛淇℃伅',0,1),(191,NULL,'user_message','edit',NULL,'淇敼鐢ㄦ埛淇℃伅',0,1),(192,NULL,'user_message','view',NULL,'鏌ョ湅鐢ㄦ埛淇℃伅',0,1),(193,NULL,'devicetype','list',NULL,'璁惧鍒楄〃鍒嗙被',0,1),(194,NULL,'devicetype','add',NULL,'娣诲姞璁惧鍒嗙被',0,1),(195,NULL,'devicetype','delete',NULL,'鍒犻櫎璁惧鍒嗙被',0,1),(196,NULL,'devicetype','edit',NULL,'淇敼璁惧鍒嗙被',0,1),(197,NULL,'devicetype','view',NULL,'鏌ョ湅璁惧鍒嗙被',0,1),(198,NULL,'projecttype','visit',NULL,'椤圭洰绫诲埆鍒楄〃',0,1),(199,NULL,'projecttype','add',NULL,'娣诲姞椤圭洰绫诲埆',0,1),(200,NULL,'projecttype','delete',NULL,'鍒犻櫎椤圭洰绫诲埆',0,1),(201,NULL,'projecttype','edit',NULL,'淇敼椤圭洰绫诲埆',0,1),(202,NULL,'projecttype','view',NULL,'鏌ョ湅椤圭洰绫诲埆',0,1),(203,NULL,'property','visit',NULL,'椤圭洰鍒楄〃',0,1),(204,NULL,'property','add',NULL,'娣诲姞椤圭洰',0,1),(205,NULL,'property','delete',NULL,'鍒犻櫎椤圭洰',0,1),(206,NULL,'property','edit',NULL,'淇敼椤圭洰',0,1),(207,NULL,'property','view',NULL,'鏌ョ湅椤圭洰',0,1),(208,NULL,'ceitinfo','add',NULL,'娣诲姞淇℃伅',0,1),(209,NULL,'ceitinfo','delete',NULL,'鍒犻櫎淇℃伅',0,1),(210,NULL,'ceitinfo','edit',NULL,'淇敼淇℃伅',0,1),(211,NULL,'ceitinfo','view',NULL,'鏌ョ湅淇℃伅',0,1),(212,NULL,'activity','visit',NULL,'娲诲姩鍒楄〃',0,1),(213,NULL,'activity','delete',NULL,'鍒犻櫎娲诲姩',0,1),(214,NULL,'activity','add',NULL,'娣诲姞娲诲姩',0,1),(215,NULL,'activity','view',NULL,'鏌ョ湅娲诲姩',0,1),(216,NULL,'account','visit',NULL,'鐣欒█鍒楄〃',0,1),(217,NULL,'account','delete',NULL,'鍒犻櫎鐣欒█',0,1),(218,NULL,'account','view',NULL,'鏌ョ湅鐣欒█',0,1),(219,NULL,'webtype','list',NULL,'鎶�鏈綉绔欏垪琛ㄥ垎绫�',0,1),(220,NULL,'webtype','add',NULL,'娣诲姞鎶�鏈綉绔欏垎绫�',0,1),(221,NULL,'webtype','delete',NULL,'鍒犻櫎鎶�鏈綉绔欏垎绫�',0,1),(222,NULL,'webtype','edit',NULL,'淇敼鎶�鏈綉绔欏垎绫�',0,1),(223,NULL,'webtype','view',NULL,'鏌ョ湅鎶�鏈綉绔欏垎绫�',0,1),(224,NULL,'web','visit',NULL,'鎶�鏈綉绔欏垪琛�',0,1),(225,NULL,'web','delete',NULL,'鍒犻櫎鎶�鏈綉绔�',0,1),(226,NULL,'web','edit',NULL,'淇敼鎶�鏈綉绔�',0,1),(227,NULL,'web','view',NULL,'鏌ョ湅鎶�鏈綉绔�',0,1),(228,NULL,'user','visit',NULL,'鐢ㄦ埛鍒楄〃',0,1),(229,NULL,'user','add',NULL,'娣诲姞鐢ㄦ埛',0,1),(230,NULL,'user','delete',NULL,'鍒犻櫎鐢ㄦ埛',0,1),(231,NULL,'user','edit',NULL,'淇敼鐢ㄦ埛',0,1),(232,NULL,'user','view',NULL,'鏌ョ湅鐢ㄦ埛',0,1),(233,NULL,'role','visit',NULL,'瑙掕壊鍒楄〃',0,1),(234,NULL,'role','add',NULL,'娣诲姞瑙掕壊',0,1),(235,NULL,'role','delete',NULL,'鍒犻櫎瑙掕壊',0,1),(236,NULL,'role','edit',NULL,'淇敼瑙掕壊',0,1),(237,NULL,'role','view',NULL,'鏌ョ湅瑙掕壊',0,1),(238,NULL,'user_message','add',NULL,'娣诲姞鐢ㄦ埛淇℃伅',0,1),(239,NULL,'user_message','get',NULL,'鑾峰彇鐢ㄦ埛淇℃伅',0,1),(240,NULL,'user_message','delete',NULL,'鍒犻櫎鐢ㄦ埛淇℃伅',0,1),(241,NULL,'user_message','edit',NULL,'淇敼鐢ㄦ埛淇℃伅',0,1),(242,NULL,'user_message','view',NULL,'鏌ョ湅鐢ㄦ埛淇℃伅',0,1),(243,NULL,'devicetype','list',NULL,'璁惧鍒楄〃鍒嗙被',0,1),(244,NULL,'devicetype','add',NULL,'娣诲姞璁惧鍒嗙被',0,1),(245,NULL,'devicetype','delete',NULL,'鍒犻櫎璁惧鍒嗙被',0,1),(246,NULL,'devicetype','edit',NULL,'淇敼璁惧鍒嗙被',0,1),(247,NULL,'devicetype','view',NULL,'鏌ョ湅璁惧鍒嗙被',0,1),(248,NULL,'projecttype','visit',NULL,'椤圭洰绫诲埆鍒楄〃',0,1),(249,NULL,'projecttype','add',NULL,'娣诲姞椤圭洰绫诲埆',0,1),(250,NULL,'projecttype','delete',NULL,'鍒犻櫎椤圭洰绫诲埆',0,1),(251,NULL,'projecttype','edit',NULL,'淇敼椤圭洰绫诲埆',0,1),(252,NULL,'projecttype','view',NULL,'鏌ョ湅椤圭洰绫诲埆',0,1),(253,NULL,'property','visit',NULL,'椤圭洰鍒楄〃',0,1),(254,NULL,'property','add',NULL,'娣诲姞椤圭洰',0,1),(255,NULL,'property','delete',NULL,'鍒犻櫎椤圭洰',0,1),(256,NULL,'property','edit',NULL,'淇敼椤圭洰',0,1),(257,NULL,'property','view',NULL,'鏌ョ湅椤圭洰',0,1),(258,NULL,'ceitinfo','add',NULL,'娣诲姞淇℃伅',0,1),(259,NULL,'ceitinfo','delete',NULL,'鍒犻櫎淇℃伅',0,1),(260,NULL,'ceitinfo','edit',NULL,'淇敼淇℃伅',0,1),(261,NULL,'ceitinfo','view',NULL,'鏌ョ湅淇℃伅',0,1),(262,NULL,'activity','visit',NULL,'娲诲姩鍒楄〃',0,1),(263,NULL,'activity','delete',NULL,'鍒犻櫎娲诲姩',0,1),(264,NULL,'activity','add',NULL,'娣诲姞娲诲姩',0,1),(265,NULL,'activity','view',NULL,'鏌ョ湅娲诲姩',0,1),(266,NULL,'account','visit',NULL,'鐣欒█鍒楄〃',0,1),(267,NULL,'account','delete',NULL,'鍒犻櫎鐣欒█',0,1),(268,NULL,'account','view',NULL,'鏌ョ湅鐣欒█',0,1),(269,NULL,'webtype','list',NULL,'鎶�鏈綉绔欏垪琛ㄥ垎绫�',0,1),(270,NULL,'webtype','add',NULL,'娣诲姞鎶�鏈綉绔欏垎绫�',0,1),(271,NULL,'webtype','delete',NULL,'鍒犻櫎鎶�鏈綉绔欏垎绫�',0,1),(272,NULL,'webtype','edit',NULL,'淇敼鎶�鏈綉绔欏垎绫�',0,1),(273,NULL,'webtype','view',NULL,'鏌ョ湅鎶�鏈綉绔欏垎绫�',0,1),(274,NULL,'web','visit',NULL,'鎶�鏈綉绔欏垪琛�',0,1),(275,NULL,'web','delete',NULL,'鍒犻櫎鎶�鏈綉绔�',0,1),(276,NULL,'web','edit',NULL,'淇敼鎶�鏈綉绔�',0,1),(277,NULL,'web','view',NULL,'鏌ョ湅鎶�鏈綉绔�',0,1),(278,NULL,'user','visit',NULL,'鐢ㄦ埛鍒楄〃',0,1),(279,NULL,'user','add',NULL,'娣诲姞鐢ㄦ埛',0,1),(280,NULL,'user','delete',NULL,'鍒犻櫎鐢ㄦ埛',0,1),(281,NULL,'user','edit',NULL,'淇敼鐢ㄦ埛',0,1),(282,NULL,'user','view',NULL,'鏌ョ湅鐢ㄦ埛',0,1),(283,NULL,'role','visit',NULL,'瑙掕壊鍒楄〃',0,1),(284,NULL,'role','add',NULL,'娣诲姞瑙掕壊',0,1),(285,NULL,'role','delete',NULL,'鍒犻櫎瑙掕壊',0,1),(286,NULL,'role','edit',NULL,'淇敼瑙掕壊',0,1),(287,NULL,'role','view',NULL,'鏌ョ湅瑙掕壊',0,1),(288,NULL,'user_message','add',NULL,'娣诲姞鐢ㄦ埛淇℃伅',0,1),(289,NULL,'user_message','get',NULL,'鑾峰彇鐢ㄦ埛淇℃伅',0,1),(290,NULL,'user_message','delete',NULL,'鍒犻櫎鐢ㄦ埛淇℃伅',0,1),(291,NULL,'user_message','edit',NULL,'淇敼鐢ㄦ埛淇℃伅',0,1),(292,NULL,'user_message','view',NULL,'鏌ョ湅鐢ㄦ埛淇℃伅',0,1),(293,NULL,'devicetype','list',NULL,'璁惧鍒楄〃鍒嗙被',0,1),(294,NULL,'devicetype','add',NULL,'娣诲姞璁惧鍒嗙被',0,1),(295,NULL,'devicetype','delete',NULL,'鍒犻櫎璁惧鍒嗙被',0,1),(296,NULL,'devicetype','edit',NULL,'淇敼璁惧鍒嗙被',0,1),(297,NULL,'devicetype','view',NULL,'鏌ョ湅璁惧鍒嗙被',0,1),(298,NULL,'projecttype','visit',NULL,'椤圭洰绫诲埆鍒楄〃',0,1),(299,NULL,'projecttype','add',NULL,'娣诲姞椤圭洰绫诲埆',0,1),(300,NULL,'projecttype','delete',NULL,'鍒犻櫎椤圭洰绫诲埆',0,1),(301,NULL,'projecttype','edit',NULL,'淇敼椤圭洰绫诲埆',0,1),(302,NULL,'projecttype','view',NULL,'鏌ョ湅椤圭洰绫诲埆',0,1),(303,NULL,'property','visit',NULL,'椤圭洰鍒楄〃',0,1),(304,NULL,'property','add',NULL,'娣诲姞椤圭洰',0,1),(305,NULL,'property','delete',NULL,'鍒犻櫎椤圭洰',0,1),(306,NULL,'property','edit',NULL,'淇敼椤圭洰',0,1),(307,NULL,'property','view',NULL,'鏌ョ湅椤圭洰',0,1),(308,NULL,'ceitinfo','add',NULL,'娣诲姞淇℃伅',0,1),(309,NULL,'ceitinfo','delete',NULL,'鍒犻櫎淇℃伅',0,1),(310,NULL,'ceitinfo','edit',NULL,'淇敼淇℃伅',0,1),(311,NULL,'ceitinfo','view',NULL,'鏌ョ湅淇℃伅',0,1),(312,NULL,'activity','visit',NULL,'娲诲姩鍒楄〃',0,1),(313,NULL,'activity','delete',NULL,'鍒犻櫎娲诲姩',0,1),(314,NULL,'activity','add',NULL,'娣诲姞娲诲姩',0,1),(315,NULL,'activity','view',NULL,'鏌ョ湅娲诲姩',0,1),(316,NULL,'account','visit',NULL,'鐣欒█鍒楄〃',0,1),(317,NULL,'account','delete',NULL,'鍒犻櫎鐣欒█',0,1),(318,NULL,'account','view',NULL,'鏌ョ湅鐣欒█',0,1),(319,NULL,'webtype','list',NULL,'鎶�鏈綉绔欏垪琛ㄥ垎绫�',0,1),(320,NULL,'webtype','add',NULL,'娣诲姞鎶�鏈綉绔欏垎绫�',0,1),(321,NULL,'webtype','delete',NULL,'鍒犻櫎鎶�鏈綉绔欏垎绫�',0,1),(322,NULL,'webtype','edit',NULL,'淇敼鎶�鏈綉绔欏垎绫�',0,1),(323,NULL,'webtype','view',NULL,'鏌ョ湅鎶�鏈綉绔欏垎绫�',0,1),(324,NULL,'web','visit',NULL,'鎶�鏈綉绔欏垪琛�',0,1),(325,NULL,'web','delete',NULL,'鍒犻櫎鎶�鏈綉绔�',0,1),(326,NULL,'web','edit',NULL,'淇敼鎶�鏈綉绔�',0,1),(327,NULL,'web','view',NULL,'鏌ョ湅鎶�鏈綉绔�',0,1);

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `permission_id` (`permission_id`),
  CONSTRAINT `permission_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `permission_role_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `permission_role` */

/*Table structure for table `project` */

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `teacher` varchar(45) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `projecttype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projecttype_id` (`projecttype_id`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`projecttype_id`) REFERENCES `projecttype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `project` */

insert  into `project`(`id`,`name`,`description`,`teacher`,`picture`,`projecttype_id`) values (2,'报告发布','报告发布','报告发布','不过爸爸',1),(3,'隔壁班','不烦不烦','拜访过不过','部分干部',1),(5,'不和你分发给','官方不会放过','报告发布','更方便地方',2);

/*Table structure for table `project_user` */

DROP TABLE IF EXISTS `project_user`;

CREATE TABLE `project_user` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `project_user_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `project_user` */

/*Table structure for table `projecttype` */

DROP TABLE IF EXISTS `projecttype`;

CREATE TABLE `projecttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `projecttype` */

insert  into `projecttype`(`id`,`name`) values (1,'哈哈'),(2,'huihui'),(4,NULL),(5,'王鹏'),(6,'呼呼'),(7,'ggggg');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isDelete` int(11) DEFAULT NULL,
  `sort` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `system` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`id`,`roleName`,`createDate`,`description`,`isDelete`,`sort`,`status`,`system`) values (9,'超级管理员','2016-06-16 16:03:39','超级管理员',0,1,1,1),(10,'普通用户','2016-06-16 16:03:40','普通用户',0,1,1,1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `account` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `create_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`account`,`description`,`create_type`) values (12,'admin','c984aed014aec7623a54f0591da07a85fd4b762d','admin',NULL,1);

/*Table structure for table `user_message` */

DROP TABLE IF EXISTS `user_message`;

CREATE TABLE `user_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `loginIp` varchar(255) DEFAULT NULL,
  `loginTime` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_message_ibfk_1` (`user_id`),
  CONSTRAINT `user_message_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `user_message` */

insert  into `user_message`(`id`,`phone`,`birthday`,`email`,`number`,`description`,`loginIp`,`loginTime`,`user_id`,`sex`,`create_time`) values (9,NULL,NULL,NULL,NULL,NULL,'127.0.0.1','2016-06-23 14:11:35',12,NULL,NULL);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

/*Table structure for table `web` */

DROP TABLE IF EXISTS `web`;

CREATE TABLE `web` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `webtype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `webtype_id` (`webtype_id`),
  CONSTRAINT `web_ibfk_1` FOREIGN KEY (`webtype_id`) REFERENCES `webtype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `web` */

/*Table structure for table `webtype` */

DROP TABLE IF EXISTS `webtype`;

CREATE TABLE `webtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `webtype` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
